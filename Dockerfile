FROM alpine:3.14 AS system
RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python
RUN python3 -m ensurepip
RUN pip3 install --no-cache --upgrade pip setuptools

FROM python:3.11-alpine as build
WORKDIR /app
ADD ./app /app
RUN apk add --no-cache \
    build-base \
    && pip install --upgrade pip
COPY requirements.txt ./
RUN pip install -r requirements.txt

FROM python:3.11-alpine as prod
COPY --from=build /usr/local/lib/python3.11/site-packages/ /usr/local/lib/python3.11/site-packages/
WORKDIR /app
ADD ./app /app
ENV PYTHONUNBUFFERED 1